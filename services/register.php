<?php
if ((function_exists('session_status') && session_status() !== PHP_SESSION_ACTIVE) || !session_id()) {
    session_start();
}
/*
if ($_SERVER["REQUEST_METHOD"] == "POST") {

echo true;
    print_r($_POST);
}
*/
$results = array (
    "success"  => false,
    "msg" => "",
    "emailValid"  => false,
    "firstnameValid"  => false,
    "lastnameValid"  => false,
    "validPassword"  => false,
    "answerValid"  => false,
    "isFistTimeUser"  => false
);
$email = $firstname = $lastname = $pwd = "";
$answer = -1;


$email = trim($_POST["email"]);
$firstname = trim($_POST["firstname"]);
$lastname =  trim($_POST["lastname"]);
$pwd = trim($_POST["pwd"]);
//$answer = (int)trim($_POST["answer"]);

if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
    $email = strtolower($email);
    $results["emailValid"] = true;
}

if (preg_match("/^.{3,100}$/", $firstname)) {
    if (preg_match("/^[a-zA-Z0-9]+$/", $firstname)) {

        $results["firstnameValid"] = true;
    }
}

if (preg_match("/^.{3,100}$/", $lastname)) {
    if (preg_match("/^[a-zA-Z0-9]+$/", $lastname)) {

        $results["lastnameValid"] = true;
    }
}
if (preg_match("/^.{5,50}$/", $pwd)) {
    if (preg_match("/^[a-zA-Z0-9]+$/", $pwd)) {
        $results["validPassword"]  = true;
    }
}
//if (isset($_SESSION['rand_code'])) {
  //  if ($answer == (int)$_SESSION['rand_code']) {
        $results["answerValid"] = true;
   // }
//}
    require_once '../internals/dbHandler.php';
$db = new DbHandler();

    if ($results["emailValid"]) {
        $results["isFistTimeUser"] = $db->isEmailNew($email);
    }

    if ($results["emailValid"] &&  $results["firstnameValid"] && $results["lastnameValid"]  &&  $results["validPassword"] && $results["answerValid"] && $results["isFistTimeUser"]) {
 
        $db->insertIntoUsersTable($email, $firstname, $lastname, $pwd);

        $results["success"] = true;
    } else {

        $results["success"] = false;

    }




header('Content-Type: application/json');
echo json_encode($results);


?>