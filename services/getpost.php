<?php
error_reporting(E_ALL);
# Check is SESSION started
if ((function_exists('session_status') && session_status() !== PHP_SESSION_ACTIVE) || !session_id()) {
    session_start();
}

require_once '../internals/dbHandler.php';

$db = new DbHandler();
$data = $db->getSinglePost($_GET["postId"]);
if (session_status() === PHP_SESSION_ACTIVE && isset($_SESSION['logedinUserName']) && isset($_SESSION['isAdmin']) && isset($_SESSION['logedinUserId']) ) {

    if($_SESSION['isAdmin'] == 1 || $data["creatorID"] ==  $_SESSION['logedinUserId'])
    {
        $data["currentUserCanUD"] = true;
    }
    else
    {
        $data["currentUserCanUD"] = false;
    }
}
header('Content-Type: application/json');
echo  json_encode($data);

?>