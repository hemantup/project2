<?php
error_reporting(E_ALL);
# Check is SESSION started
if ((function_exists('session_status') && session_status() !== PHP_SESSION_ACTIVE) || !session_id()) {
    session_start();
}
$results = array (
    "success"  => false,
    "msg" => ""

);


if (session_status() === PHP_SESSION_ACTIVE && isset($_SESSION['logedinUserName']) && isset($_SESSION['isAdmin']) && isset($_SESSION['logedinUserId']) ) {
    require_once '../internals/dbHandler.php';

    $db = new DbHandler();
    $data = $db->getSinglePost($_POST["postId"]);
    if($_SESSION['isAdmin'] == 1 || $data["creatorID"] ==  $_SESSION['logedinUserId'])
    {
        $deldata = $db->deleteSinglePost($_POST["postId"]);
        $results["success"] = true;
    }
    else
    {
        $results["msg"] = "Insufficient permissions.";
    }
}
else{

    $results["msg"] = "No Session is active";
}

header('Content-Type: application/json');
echo  json_encode($results);

?>