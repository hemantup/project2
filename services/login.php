<?php
if ((function_exists('session_status') && session_status() !== PHP_SESSION_ACTIVE) || !session_id()) {
    session_start();
}
require_once '../internals/dbHandler.php';
$results = [];
$success = false;
$isAdmin = false;
$name = "";
$email = "";
$id = "";
$msg = "";
$db = new DbHandler();
$data = $db->retriveValidUser(trim(strtolower($_POST["email"])), trim($_POST["pwd"]));

$success = !is_null($data);

if($success){
    $isAdmin =  $data['isadmin'];
    $name = $data['firstname']." ".$data['lastname'];
    $email = $data['email'];
    $id = $data['id'];
    $_SESSION['isAdmin'] = $isAdmin;
    $_SESSION['logedinUserName'] = $name ;
    $_SESSION['logedinUserEmail'] = $email ;
    $_SESSION['logedinUserId'] = $id ;



}
else
{
    $msg = "email and password combination wrong.";
}

$results['success'] = $success;
$results['msg'] =  $msg;
header('Content-Type: application/json');
echo json_encode($results);


?>