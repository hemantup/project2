<?php

class DbHandler
{

    private $conn;

    function __construct()
    {
        require_once dirname(__FILE__) . '/connect.php';
        // opening db connection
        $db = new dbConnect();
        $this->conn = $db->connect();
    }
    public function getAllPosts()
    {
        $results = [];
        $r = $this->conn->query('select posts.id as postid, CONCAT(authorlink.firstname, \' \', authorlink.lastname) as author, CONCAT(editorlink.firstname, \' \', editorlink.lastname) as editor, posts.title as subject, posts.description as summary FROM posts join users as authorlink on posts.author = authorlink.id join users as editorlink on posts.editor = editorlink.id where posts.isdeleted = 0  ORDER BY posts.modified desc') or die($this->conn->error . __LINE__);

//return $results  =  $r->fetch_all(); // this works in local xmapp, but fails on goDaddy hosting

        while ($rr = $r->fetch_assoc()) {
            array_push($results, $rr);
        }
        return $results;


    }

    public function getSinglePost($postId)
    {
        $results = [];

        $r = $this->conn->query('select posts.id as postid, posts.author as creatorID, CONCAT(authorlink.firstname, \' \', authorlink.lastname) as author, CONCAT(editorlink.firstname, \' \', editorlink.lastname) as editor, posts.title as subject, posts.description as summary , posts.content as content, posts.created as created , posts.modified as modified FROM posts join users as authorlink on posts.author = authorlink.id join users as editorlink on posts.editor = editorlink.id where posts.isdeleted = 0  and posts.id = '. $postId) or die($this->conn->error . __LINE__);

        $results = $r->fetch_assoc();

        return $results;

    }




    public function createPost($authorandeditor, $content, $description, $title)
    {

        $content = $this->conn->real_escape_string($content);
        $description = $this->conn->real_escape_string($description);
        $title = $this->conn->real_escape_string($title);

        $query = "INSERT INTO posts (author , editor,  content , description, title, created ) VALUES ('" . $authorandeditor . "', '" . $authorandeditor . "', '" . $content . "', '" . $description . "', '" . $title . "', CURRENT_TIMESTAMP)";

        $r = $this->conn->query($query) or die($this->conn->error . __LINE__);

    }

    public function deleteSinglePost($postId)
    {
        $query = "DELETE FROM posts WHERE posts.id = " . $postId;

        $r = $this->conn->query($query) or die($this->conn->error . __LINE__);

    }


    public function editSinglePost($editor, $postId, $content, $description, $title)
    {

        $content = $this->conn->real_escape_string($content);
        $description = $this->conn->real_escape_string($description);
        $title = $this->conn->real_escape_string($title);

        $query = "UPDATE posts SET title = '".$title."', description = '".$description."', content = '".$content."', editor = ".$editor." WHERE posts.id = ".$postId;

        $r = $this->conn->query($query) or die($this->conn->error . __LINE__);

    }


    public function retriveValidUser($email, $pwd)
    {
        $results = [];

        $r = $this->conn->query('select * from users where email = \''.$email.'\' and pwd = \''.$pwd.'\'') or die($this->conn->error . __LINE__);

        $results = $r->fetch_assoc();

        return $results;

    }
    public function insertIntoUsersTable($email, $firstname, $lastname, $pwd)
    {


        $query = "INSERT INTO users (email ,  firstname , lastname, pwd) VALUES ('" . $email . "', '" . $firstname . "', '" . $lastname . "', '".$pwd."')";

        $r = $this->conn->query($query) or die($this->conn->error . __LINE__);

    }

    public function isEmailNew($email)
    {
        $r = $this->conn->query("SELECT email FROM users where email = '" . $email . "'   Limit 1") or die($this->conn->error . __LINE__);
        //   return $r->num_rows == 0  ; // this works in local xmapp, but fails on goDaddy hosting
        $result = $r->fetch_assoc();
        return empty($result);
    }


}



?>