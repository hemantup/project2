﻿<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Project 2 By Hemant">
    <meta property="og:description" content="Project 2 By Hemant" />
    <meta property="og:title" content="Project 2 By Hemant" />
    <meta property="og:image" content="https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/Fort_Hays_State_University.jpg" />
    <meta property="og:image:width" content="250" />
    <meta property="og:image:height" content="250" />
    <meta property="og:url" content="https://www.fhsu.edu/" />
    <meta property="og:site_name" content="PHP Project by Hemant" />
    <meta name="classification" content="PHP Project by Hemant" />
    <meta name="subject" content="PHP Project by Hemant" />
    <meta name="keywords" content="PHP, AnugularJS, Hemant" />
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.2/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.2/angular-route.min.js"></script>
    <title>PHP Project by Hemant</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



</head>
<body ng-app="testingapp">
<script src="scripts/myapp.js"></script>
<script src="scripts/visitorlocation.js"></script>
<script src="scripts/allpostsctrl.js"></script>
<script src="scripts/viewpostctrl.js"></script>
<script src="scripts/loginctrl.js"></script>
<script src="scripts/logoutctrl.js"></script>
<script src="scripts/createpostctrl.js"></script>
<script src="scripts/editpostctrl.js"></script>
<script src="scripts/registerctrl.js"></script>

<script src="scripts/searchip.js"></script>
<div id="wrapper" >
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#/intro">Project 2 By Hemant</a>

        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li style="visibility: hidden;"> <button onclick="ReloadMyApplication();" id="relaodAppButton"  style="visibility: hidden;"></button></li>
            <li>

                <?php

                if (session_status() === PHP_SESSION_ACTIVE && isset($_SESSION['logedinUserName']) ) {

                    echo "<span style=\"color:white;\" >Welcome ".$_SESSION['logedinUserName']." </span>
                <a href=\"#/logout\" >Logout</a>";

                }
                else
                {

                    echo "<a href=\"#/login\" >Login</a>" ;
                }

                ?>
            </li>



        </ul>
        <div class="navbar-inverse sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">

                <ul class="nav" id="side-menu">

                    <li>

                        <a href="#/intro">Introduction</a>
                    </li>

                    <li>

                        <a href="#/home">All Posts</a>
                    </li>
                    <?php

                    if (session_status() === PHP_SESSION_ACTIVE && isset($_SESSION['logedinUserName']) ) {

                        echo "  <li>
  <a href=\"#/create\">Create New Post</a>
   </li>";

                    }
else{
    echo "  <li>
  <a href=\"#/signup\">Register</a>
   </li>";
}

                    ?>
                    <li>
                        <a href="#/yourlocation">Get Your Location</a>
                    </li>
                    <li>
                        <a href="#/searchanip">Search Domain or IP Location</a>
                    </li>


                </ul>
            </div>
        </div>
    </nav>
    <div ng-view></div>
</div><!-- /#wrapper -->
</body>
</html>
